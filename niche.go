package main

import (
	"github.com/gdamore/tcell"
	"niche/tracker"
	"time"
)

var style = tcell.StyleDefault

type cursorPos struct {
	X    int
	Y    int
	minX int
	maxX int
	minY int
	maxY int
}

func e(err error) {
	panic(err.Error())
}

func main() {
	var cursor = new(cursorPos)
	s, err := tcell.NewScreen()
	if err != nil {
		e(err)
	}
	err = s.Init()
	if err != nil {
		e(err)
	}
	s.EnableMouse()
	clock := tracker.NewClock(120)
	go clock.Start()

	pattern := tracker.NewPattern(clock)

	quit := make(chan struct{})
	x, y, w, h := pattern.GetTrackWindowSize()
	cursor.SetBoundaries(x, y, w, h)
	sTicker := time.NewTicker(33333 * time.Microsecond) // TODO: Make Value Constant

loop:
	for {
		select {
		case <-quit:
			clock.Stop()
			break loop
		case <-sTicker.C:
			go handler(s, quit, cursor, style, &pattern)
			go drawScreen(s, cursor, pattern)
		}
	}
	s.Fini()
}

func drawScreen(s tcell.Screen, cur *cursorPos, p tracker.Pattern) {
	cur.JailCursor()
	s.ShowCursor(cur.X, cur.Y)
	p.DisplayPattern(s, style, colors)
	s.Show()
}
