package main

// Handles all keyboard events.
import (
	"github.com/gdamore/tcell"
	"niche/tracker"
	//"goker/tracker"
)

// Longboi function. Merely a giant switch/case set up to pase individual key events.
func parseKey(s tcell.Screen, run *tcell.EventKey, cur *cursorPos, quit chan struct{}, p *tracker.Pattern) {
	switch run.Key() {
	case tcell.KeyEscape, tcell.KeyEnter:
		close(quit)
		return
	case tcell.KeyCtrlL:
		s.Sync()
	case tcell.KeyDown:
		cur.Y += 1
	case tcell.KeyUp:
		cur.Y -= 1
	case tcell.KeyLeft:
		cur.X -= 1
		skipSpace(s, cur, "bwd")
	case tcell.KeyRight:
		cur.X += 1
		skipSpace(s, cur, "fwd")
	}
	switch run.Rune() {
	case 'j':
		cur.Y += 1
	case 'k':
		cur.Y -= 1
	case 'h':
		cur.X -= 1
		skipSpace(s, cur, "bwd")
	case 'l':
		cur.X += 1
		skipSpace(s, cur, "fwd")
	case 'w':
		skipWord(s, cur, "fwd")
		skipSpace(s, cur, "fwd")
	case 'b':
		skipWord(s, cur, "bwd")
		skipSpace(s, cur, "bwd")
	case 'r':
		//p.RandomizeCell(cur.X, cur.Y)
	case 'f':
		s.Fill('f', 8)
	case ' ':
		p.Play()
	case 'q':
		close(quit)
		return
	}
}

//Helper function to skip forward to the next Col
func nextCol(s tcell.Screen, cur *cursorPos) {
	c, _, _, _ := s.GetContent(cur.X, cur.Y)
	for c != ' ' && c != tcell.RuneVLine {
		cur.X += 1
	}
	if cur.X >= cur.maxX {
		cur.X = cur.minX
		cur.Y += 1
	}
}

// Helper Function to skip spaces when encountered
func skipSpace(s tcell.Screen, cur *cursorPos, dir string) {
	c, _, _, _ := s.GetContent(cur.X, cur.Y)
	for c == ' ' || c == tcell.RuneVLine && cur.X > cur.minX && cur.X < cur.maxX {
		if dir == "bwd" {
			cur.X -= 1
		} else {
			cur.X += 1
		}
		c, _, _, _ = s.GetContent(cur.X, cur.Y)
	}
}

// Helper Function to skip entire coluns for w, b movements
func skipWord(s tcell.Screen, cur *cursorPos, dir string) {
	c, _, _, _ := s.GetContent(cur.X, cur.Y)
	for c != ' ' && c != tcell.RuneVLine {
		if dir == "bwd" {
			cur.X -= 1
			if cur.X <= cur.minX {
				cur.Y -= 1
				cur.X = cur.maxX
			}
		} else {
			cur.X += 1
			if cur.X >= cur.maxX {
				cur.Y += 1
				cur.X = cur.minX
			}
		}
		c, _, _, _ = s.GetContent(cur.X, cur.Y)
	}
}
