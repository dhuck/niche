package main

import (
	"github.com/gdamore/tcell"
	"niche/tracker"
)

// Main function to parse mouse events. Only supports location clicking at the moment
func parseMouse(s tcell.Screen, event *tcell.EventMouse, cur *cursorPos) {
	button := event.Buttons()
	switch button {
	case tcell.Button1:
		x, y := event.Position()
		cur.X = x
		cur.Y = y
		skipSpace(s, cur, "fwd")
	}
}

// Detects and parses all events from the user.
func handler(s tcell.Screen, quit chan struct{}, cur *cursorPos, bg tcell.Style, p *tracker.Pattern) {
	ev := s.PollEvent()
	switch ev := ev.(type) {
	case *tcell.EventKey:
		parseKey(s, ev, cur, quit, p)
	case *tcell.EventMouse:
		parseMouse(s, ev, cur)
	case *tcell.EventResize:
		s.Fill(' ', bg)
		s.Sync()
	}
}

// Sets the boundary of where the cursor can reasonably be.
func (c *cursorPos) SetBoundaries(x, y, w, h int) {
	c.minX, c.minY, c.maxX, c.maxY = x, y, x+w, y+h
}

// Keeps the cursor within the provided boundaries.
func (c *cursorPos) JailCursor() {

	if c.X < c.minX {
		c.X = c.minX
	}
	if c.Y < c.minY {
		c.Y = c.minY
	}
	//if c.X > c.maxX { c.X = c.maxX - 1}
	//if c.Y > c.minY { c.Y = c.maxY - 1}
}

// TODO: Write interactive Mode
