package tracker

import (
	"fmt"
	"github.com/gdamore/tcell"
)

// TODO: Set a way to mark the active row
// Track is a child of Pattern and the parent of rows. Simply an array of rows with helper functions
type track struct {
	id        int
	pos       position
	name      []rune
	rows      []row
	fxEnabled int
	header    []rune

	tpl   int // Ticks Per Line
	clk   *clock
	pulse chan interface{}
	stop  chan interface{}
	play  bool
}

// Constructor. Requires explicit length, x & y pos, id and pointer to the clock.
// TODO: Normalize to cell parameter list
func newTrack(length, pos, x, y int, c *clock) track {
	var out = track{}
	out.id = pos
	out.pos.length = length
	out.fxEnabled = DefaultFxEnabled

	out.rows = make([]row, length)
	out.name = make([]rune, 0)
	for i := 0; i < length; i++ {
		out.rows[i] = newRow(i, x, y)
	}
	out.rows[0].activate()

	out.header = out.makeHeader()
	out.pos.width = len(out.header)

	out.pos.x = x
	out.pos.y = y // beginning of track, discludes header
	out.pulse = make(chan interface{}, 5)
	out.clk = c
	out.tpl = 6
	return out
}

// Get current width of the track
func (t *track) GetWidth() int {
	return t.pos.width
}

// Create the header for the track dependant on number of FX shown / hidden
func (t *track) makeHeader() []rune {
	base := "NOT ST VL "
	for i := 0; i < t.fxEnabled; i++ {
		if i != 0 {
			base += " "
		}
		base += fmt.Sprintf("FX %02X", i)

	}
	var out []rune
	for _, c := range base {
		out = append(out, c)
	}
	return out
}

// Displays the Tracks. Calls each row to call and display information while handling background colors.
func (t *track) DisplayTrack(s tcell.Screen, x, y int, style tcell.Style, theme map[string]tcell.Color) {
	t.pos.x = x
	t.pos.y = y
	orig_style := style
	for _, row := range t.rows {
		if row.id%((TPQN/t.tpl)*4) == 0 {
			style = style.Background(theme["StrongerRow"])
		} else if row.id%((TPQN/t.tpl)*2) == 0 {
			style = style.Background(theme["StrongRow"])
		} else if row.id%(TPQN/t.tpl) == 0 {
			style = style.Background(theme["WeakRow"])
		} else {
			style = orig_style
		}
		row.displayCell(s, x, y, style, theme)
		y += 1
	}
}

// Subsribes the track to the clock and waits for a stop message to exit.
func (t *track) Play() {
	i := 0
	pulses := 0
	ticks := 0
	t.pulse = t.clk.Subscribe()
	for {
		select {
		case <-t.stop:
			return
		case <-t.pulse:
			if pulses%TickConverter == 0 {
				if ticks%t.tpl == 0 {
					t.rows[i].deactivate()
					i++
					if i == t.pos.length {
						i = 0
						pulses = 0
					}
					t.rows[i].activate()
				}
				ticks++
			}
			pulses++
		}
	}
}

// Deactivates the rows, and unsubscribes the clock.
func (t *track) Stop() {
	t.pulse <- t.stop
	for i, _ := range t.rows {
		t.rows[i].deactivate()
	}
	t.clk.Unsubsribe(t.pulse)
	t.pulse = make(chan interface{}, 5)
}
