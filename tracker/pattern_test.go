package tracker

import "testing"

func TestNewPattern(t *testing.T) {
	clk := NewClock(120)
	pat := NewPattern(clk)
	tracks := pat.tracks
	baseTrack := 4
	baseTrackWidth := (3 * RegularCols) + (5 * DefaultFxEnabled) + 1
	lineWidth := 1
	for i, track := range tracks {
		if track.GetWidth() != baseTrackWidth { // get down to 21
			t.Errorf("Track width incorrect, Expected: %d, Received: %d", baseTrackWidth, track.GetWidth())
		}
		expected := baseTrack + (i * (baseTrackWidth + lineWidth))
		if track.pos.x != expected {
			t.Errorf("Track %d is misplaced! Expected: %d, Received: %d", i, expected, track.pos.x)
		}
	}

}
