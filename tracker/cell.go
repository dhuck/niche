package tracker

// Cell Struct is not meant to be called directly, but is a child of the Row Class.
// Everything within is called by Row

type cell struct {
	id     int8
	val    int8
	pos    position
	hidden bool
	dtype  string
}

// Constructor. Requires x, y position and an id number from Row.
func newCell(x, y, id int) cell {
	pos := position{x, y, -1, 3}
	out := cell{int8(id), -1, pos, true, ""}

	return out
}

// Set value to cell
func (c *cell) setVal(val int) {
	if val >= -1 && val <= 127 {
		c.val = int8(val)
	}
	// TODO: Handle erroneous values
}

// Gets value from cell
func (c *cell) getVal() int {
	return int(c.val)
}

// Set the Cell Position
func (c *cell) setCellPos(x, y int) {
	c.pos.x, c.pos.y = x, y
}

// Retreive Cell Position
func (c *cell) getCellPos() (int, int) {
	return c.pos.x, c.pos.y
}

// Toggles visibility. Planned usage is adding/subtracting FX Tracks at a later date.
func (c *cell) toggleVisible() {
	c.hidden = !c.hidden
}

// Show a hidden cell.
func (c *cell) show() {
	c.hidden = false
}

// Hide a hidden cell
func (c *cell) hide() {
	c.hidden = true
}

func (c *cell) setType(dtype string) {
	c.dtype = dtype
}
func (c *cell) getType() string {
	return c.dtype
}
