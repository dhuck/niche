package tracker

import "time"

// Constants
const PPQN = 240
const MaxBPM = 300
const MinBPM = 25
const TPQN = 24
const TickConverter = PPQN / TPQN // PPQN / TickConverter MUST EQUAL 24
const Microsecond = 1000000       // Conversion factor for milliseconds
const Minute = 60                 // Seconds

// Clock uses a broker type structure where clients can subscribe and unsubscribe as needed. Each track will be a
// subscriber to the clock, allowing for polyrhythmic patterns to arise naturally.
type clock struct {
	tempo int
	pulse *time.Ticker

	stopCh    chan struct{}
	publishCh chan interface{}
	subCh     chan chan interface{}
	unsubCh   chan chan interface{}

	subs map[chan interface{}]struct{}
}

// Constructor
func NewClock(bpm int) *clock {
	if bpm > MaxBPM {
		bpm = MaxBPM
	} else if bpm < MinBPM {
		bpm = MinBPM
	}

	out := clock{
		tempo:     bpm,
		pulse:     time.NewTicker(time.Duration(Microsecond/(bpm/Minute)/PPQN) * time.Microsecond),
		stopCh:    make(chan struct{}),
		publishCh: make(chan interface{}, 1),
		subCh:     make(chan chan interface{}, 1),
		unsubCh:   make(chan chan interface{}, 1),
		subs:      make(map[chan interface{}]struct{}),
	}
	return &out
}

// Starts the Clock. Listens for subscriptions, unsubscriptions, and stop messages.

func (c clock) Start() {
	for {
		select {
		case <-c.stopCh:
			return
		case msgCh := <-c.subCh:
			c.subs[msgCh] = struct{}{}
		case msgCh := <-c.unsubCh:
			delete(c.subs, msgCh)
		case msg := <-c.pulse.C:
			for msgCh := range c.subs {
				select {
				case msgCh <- msg:
				}
			}
		}
	}
}

// Experimental. Stops the clock. Most likely will remain unused because midi clock.
func (c *clock) Stop() {
	close(c.stopCh)
}

// Subscribe to the Clock. Returns a channel for the subscribing track to listen to.
func (c *clock) Subscribe() chan interface{} {
	msgCh := make(chan interface{}, 5)
	c.subCh <- msgCh
	return msgCh
}

// Track unsubscribe. Current method for stopping playback.
func (c *clock) Unsubsribe(msgCh chan interface{}) {
	c.unsubCh <- msgCh
}
