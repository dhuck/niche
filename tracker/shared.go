package tracker

// Constants
const DefaultTrackCount = 4
const DefaultTrackLength = 32
const DefaultFxEnabled = 1
const HeaderSize = 3
const RowNumWidth = 4
const TrackBuffer = 1
const RegularCols int = 3
const FxCount int = 12
const NotesPerOctave int8 = 12

// Lookup Array for the Column names by number
var Columns = []string{"NOT", "ST", "VL", "FX 00", "FX 01", "FX 02", "FX 03", "FX 04", "FX 05", "FX 06", "FX 07", "FX 08",
	"FX 09", "FX 0A", "FX 0B", "FX 0C", "FX 0D", "FX 0E", "FX 0F"}

// Lookup Map for Column number by header title.
var ColumnMap = map[string]int{
	"NOT":   0,
	"ST":    1,
	"VL":    2,
	"FX 00": 3,
	"FX 01": 5,
	"FX 02": 7,
	"FX 03": 9,
	"FX 04": 11,
	"FX 05": 13,
	"FX 06": 15,
	"FX 07": 17,
	"FX 08": 19,
	"FX 09": 21,
	"FX 0A": 23,
	"FX 0B": 25,
	"FX 0C": 27,
	"FX 0D": 29,
	"FX 0E": 31,
	"FX 0F": 33,
}

// Lookup array for note names by note number
var Notes = [NotesPerOctave]string{"C-", "C#", "D-", "D#", "E-", "F-", "F#", "G-", "G#", "A-", "A#", "B-"}

// Active struct for Pattern
type active struct {
	track int
	row   int
	col   int
}

// Position struct used by tracks, rows, and cells
type position struct {
	x      int
	y      int
	length int
	width  int
}
