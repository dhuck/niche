package tracker

import "testing"

// Test the constructor
func TestNewRow(t *testing.T) {

}

func TestRow_SetNote(t *testing.T) {
	got := newRow(0, 0, 0)
	legalNumbers := []int{54, 60, 23, 64, 1, 4, 16, 127, -1}
	illegalNumbers := []int{-2, 128, 256, 5234, -43, -65}
	for _, val := range legalNumbers {
		got.SetNote(val)
		if val != got.cells[ColumnMap["NOT"]].getVal() {
			t.Errorf("SetVol Failed! Expected: %d, received: %d",
				val, got.cells[ColumnMap["NOT"]].getVal())
		}
	}
	for _, val := range illegalNumbers {
		got.SetVol(val)
		if got.cells[ColumnMap["NOT"]].getVal() != -1 {
			t.Errorf("SetVol Failed! Expected: %d, received: %d",
				-1, got.cells[ColumnMap["NOT"]].getVal())
		}
	}
}

func TestRow_SetVol(t *testing.T) {
	legalNumbers := []int{54, 60, 23, 64, 1, 4, 16, 127, -1}
	illegalNumbers := []int{-2, 128, 256, 5234, -43, -65}
	got := newRow(0, 0, 0)
	for _, val := range legalNumbers {
		got.SetVol(val)
		if val != got.cells[ColumnMap["VL"]].getVal() {
			t.Errorf("SetVol Failed! Expected: %d, received: %d",
				val, got.cells[ColumnMap["VL"]].getVal())
		}
	}
	for _, val := range illegalNumbers {
		got.SetVol(val)
		if got.cells[ColumnMap["VL"]].getVal() != -1 {
			t.Errorf("SetVol Failed! Expected: %d, received: %d",
				-1, got.cells[ColumnMap["VL"]].getVal())
		}
	}
}

func TestRow_SetSlot(t *testing.T) {
	legalNumbers := []int{54, 60, 23, 64, 1, 4, 16, 127, -1}
	illegalNumbers := []int{-2, 128, 256, 5234, -43, -65}
	got := newRow(0, 0, 0)
	for _, val := range legalNumbers {
		got.SetSlot(val)
		if val != got.cells[ColumnMap["ST"]].getVal() {
			t.Errorf("SetSlot Failed! Expected: %d, received: %d", val, got.cells[ColumnMap["ST"]].getVal())
		}
	}
	for _, val := range illegalNumbers {
		got.SetSlot(val)
		if got.cells[ColumnMap["ST"]].getVal() != -1 {
			t.Errorf("SetSlot Failed! Expected: %d, received: %d", -1, got.cells[ColumnMap["ST"]].getVal())
		}
	}
}

func TestRow_GetNote(t *testing.T) {
	got := newRow(0, 0, 0)
	got.SetNote(64)
	note := got.GetNote()
	if note != 64 {
		t.Errorf("Get Note Failed; expected: %d; but received: %d", 64, note)
	}
}

func TestRow_GetSlot(t *testing.T) {
	got := newRow(0, 0, 0)
	got.SetSlot(64)
	note := got.GetSlot()
	if note != 64 {
		t.Errorf("Get Note Failed; expected: %d; but received: %d", 64, note)
	}
}

func TestRow_GetVol(t *testing.T) {
	got := newRow(0, 0, 0)
	got.SetVol(64)
	note := got.GetVol()
	if note != 64 {
		t.Errorf("Get Note Failed; expected: %d; but received: %d", 64, note)
	}
}

func TestRow_OutString(t *testing.T) {
	got := newRow(0, 0, 0)
	got.SetNote(60)
	got.SetSlot(02)
	got.SetVol(127)
	if got.OutString("NOT") != "C-5" {
		t.Errorf("Expected: C-5, received: %s", got.OutString("Note"))
	}
	if got.OutString("ST") != "02" {
		t.Errorf("Expected:  02, received: %s", got.OutString("Slot"))
	}
	if got.OutString("VL") != "7F" {
		t.Errorf("Expected: -FF, received: %s", got.OutString("Vol"))
	}
}
