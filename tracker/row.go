package tracker

import (
	"fmt"
	"github.com/gdamore/tcell"
)

// Row is a parent to the cells and a child of track. Simply a list of cells with helper functions to drive them.

type row struct {
	id        int
	pos       position
	cells     []cell
	active    bool
	fxEnabled int
}

// Constructor, requires x, y position and id from caller.
// TODO: Normalize position of the parameters to the Cell constructor.
func newRow(id, x, y int) row {
	cellCount := 2*FxCount + RegularCols
	pos := position{x, y, 1, 0}
	cells := make([]cell, 0)
	for i := 0; i < cellCount; i++ {
		cells = append(cells, newCell(x, y, i))
		if i == 0 {
			cells[i].setType("Note")
		} else if i == 1 {
			cells[i].setType("Slot")
		} else if i == 2 {
			cells[i].setType("Vol")
		} else if i%2 == 1 {
			cells[i].setType("FxType")
		} else {
			cells[i].setType("FxAmt")
		}
	}
	for i := 0; i < (RegularCols + (DefaultFxEnabled * 2)); i++ {
		cells[i].show()
	}
	for i := 0; !cells[i].hidden; i++ {
		pos.width += 3
	}
	out := row{id, pos, cells, false, DefaultFxEnabled}

	return out
}

// Special logic to handle note setting. Can be calledw with 1 or 2 parameters.
// Two parameters will be for note / octave. One Parameter will set note on value between
// 1 and 127.
// TODO: Numerical checking for note value in note / octave format
func (r *row) SetNote(x ...int) {
	if len(x) == 2 { // sent in note, octave pair
		note := x[0]
		octave := x[1]
		midiNote := note + octave*int(NotesPerOctave)
		r.cells[ColumnMap["NOT"]].setVal(midiNote)
	} else {
		r.cells[ColumnMap["NOT"]].setVal(x[0])
	}
}

// Set the "Slot" Column value
func (r *row) SetSlot(slot int) {
	r.cells[ColumnMap["ST"]].setVal(slot)
}

// Set the "Volume / Velocity" Column
func (r *row) SetVol(vol int) {
	r.cells[ColumnMap["VL"]].setVal(vol)
}

// Retreive the note as integer 1 - 127
func (r *row) GetNote() int {
	return r.cells[0].getVal()
}

// Retreive the Slot
func (r *row) GetSlot() int {
	return r.cells[1].getVal()
}

// Retreive the Volume
func (r *row) GetVol() int {
	return r.cells[2].getVal()
}

// Set an effect type. Requires which effect column and type of effect.
func (r *row) SetFxType(fct, fx int) {
	fct = (fct * 2) + RegularCols
	r.cells[fct].setVal(fx)
}

// Set effect amount. Requires which effect and value between 1 and 127 for amount
func (r *row) SetFxAmt(fct, amt int) {
	fct = (fct * 2) + RegularCols + 1
	r.cells[fct].setVal(amt)
}

// Helper function to create information display to the screen.
func (r *row) OutString(field string) string {
	switch field {
	case "NOT":
		if r.cells[ColumnMap[field]].getVal() >= 0 {
			note, octave := r.makeNoteOctave(r.cells[ColumnMap[field]].getVal())
			return fmt.Sprintf("%s%X", Notes[note], octave)
		} else {
			return "---"
		}
	case "ST":
		if r.cells[ColumnMap[field]].getVal() != -1 {
			return fmt.Sprintf("%02X", r.cells[ColumnMap[field]].getVal())
		} else {
			return "--"
		}
	case "VL":
		if r.cells[ColumnMap[field]].getVal() != -1 {
			return fmt.Sprintf("%02X", r.cells[ColumnMap[field]].getVal())
		} else {
			return "--"
		}
	case "FX":
		outStr := ""
		for i := RegularCols; !r.cells[i].hidden; i += 2 {
			if r.cells[i].getVal() != -1 {
				outStr += fmt.Sprintf("%02X ", r.cells[i].getVal())
				outStr += fmt.Sprintf("%02X ", r.cells[i+1].getVal())
			} else {
				outStr += "-- -- "
			}
		}
		return outStr
	default:
		return ""
	}
}

// Drives OutString() function to feed information up to Track to be displayed by Pattern
func (r *row) displayCell(s tcell.Screen, x, y int, style tcell.Style, theme map[string]tcell.Color) {
	order := []string{"NOT", "ST", "VL", "FX"}
	if r.active {
		style = style.Background(theme["ActiveRow"])
	}
	for _, field := range order {
		fieldOut := r.OutString(field)
		for i, char := range fieldOut {
			fieldStyle := style.Foreground(theme[field]).Bold(true)
			s.SetContent(x+i, y, char, nil, fieldStyle)
		}
		if field != "FX" {
			x += len(fieldOut + " ")
			s.SetContent(x-1, y, ' ', nil, style)
		}
	}
}

// Helper file Candidates
func (r *row) makeNoteOctave(pitch int) (int, int) {
	note := pitch % int(NotesPerOctave)
	octave := pitch / int(NotesPerOctave)

	return note, octave
}

func (r *row) activate() {
	r.active = true
}

func (r *row) deactivate() {
	r.active = false
}
