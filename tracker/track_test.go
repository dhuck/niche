package tracker

import "testing"

func TestNewTrack(t *testing.T) {
	defaultHeader := "NOT ST VL FX 00 FX 01"
	var headRune []rune
	for _, c := range defaultHeader {
		headRune = append(headRune, c)
	}
	clk := NewClock(120)
	got := newTrack(64, 0, 0, 0, clk)
	if got.pos.length != 64 {
		t.Errorf("Unable to set track length in constructor")
	}
	if got.id != 0 {
		t.Errorf("Unable to set track position in constructor")
	}
	if len(got.rows) != 64 {
		t.Errorf("Cells not properly created in Constructor")
	}
	for _, r := range got.rows {
		if r.GetVol() != -1 {
			t.Errorf("Cells not properly created in Constructor")
		}
	}
	if got.fxEnabled != DefaultFxEnabled {
		t.Errorf("FxEnabled not proper number in Constructor")
	}
	for i, c := range got.header {
		if got.header[i] != headRune[i] {
			t.Errorf("Header mismatch at index %d", i)
			t.Errorf("Expected: %s; Received: %s", string(headRune[i]), string(c))

		}
	}
}
