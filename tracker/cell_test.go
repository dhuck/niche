package tracker

import "testing"

func TestNewCell(t *testing.T) {
	c := newCell(0, 1, 0)
	legalNum := []int{0, 64, 127, -1}
	illegalNum := []int{-2, 128, 256, -643}
	for _, v := range legalNum {
		c.setVal(v)
		if c.getVal() != v {
			t.Errorf("Incorrect Cell Value, expected: %d, got: %d", v, c.getVal())
		}
	}
	for _, v := range illegalNum {
		c.setVal(v)
		if c.getVal() != -1 {
			t.Errorf("Incorrect Cell Value, expected: -1, got: %d", c.getVal())
		}
	}
}
