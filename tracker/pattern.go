package tracker

import (
	"fmt"
	"github.com/gdamore/tcell"
	"math/rand"
	"time"
)

// Pattern is the current super structure of the project. Will be supplanted by Projects.
type Pattern struct {
	pos       position
	rows      int
	tracks    []track
	activeCol [3]int

	play   bool
	cursor active
	pulse  *clock
}

// Constructor. Only expects a pointer to a clock as a parameter
func NewPattern(c *clock) Pattern {
	out := Pattern{}
	out.rows = DefaultTrackLength
	// Top used to add spacing for header
	out.pos.x = 0
	out.pos.y = 0
	out.cursor = active{0, 0, 0}
	out.pos.length = out.rows + HeaderSize

	width := 0
	for i := 0; i < DefaultTrackCount; i++ {
		out.tracks = append(out.tracks, newTrack(out.rows, i, RowNumWidth+width, HeaderSize, c))
		width += out.tracks[i].GetWidth() + 1
		//go out.tracks[i].Play()
	}
	out.pos.width = width
	out.pulse = c
	out.play = false

	return out
}

// Starts / Stops the pattern by subscribing/unsubscribing all children tracks to the Clock.
func (p *Pattern) Play() {
	if p.play {
		for i, _ := range p.tracks {
			go p.tracks[i].Stop()
		}
	} else {
		for i, _ := range p.tracks {
			go p.tracks[i].Play()
		}
	}
	p.play = !p.play
}

//func (p *Pattern) Stop() {
//	for _, track := range p.tracks {
//		track.Stop()
//	}
//}

// Display Pattern. Requires screen object, style, and theme.
// TODO: Refactor into helper functions.
func (p *Pattern) DisplayPattern(s tcell.Screen, style tcell.Style, theme map[string]tcell.Color) {
	for i := 0; i < p.rows; i++ {
		row := fmt.Sprintf("%03X", i)
		for j, c := range row {
			s.SetContent(p.pos.x+j, i+p.pos.y+HeaderSize, c, nil, style)
		}
	}

	trackBoxX := p.pos.x + RowNumWidth
	trackBoxY := p.pos.y + HeaderSize
	p.drawVLine(s, trackBoxX-1, p.pos.y, p.pos.length, style, "left")
	for _, track := range p.tracks {
		p.drawHLine(s, trackBoxX, p.pos.y, track.GetWidth(), style)      // draw top line
		p.drawHLine(s, trackBoxX, HeaderSize-1, track.GetWidth(), style) // divide the from rest
		p.drawHLine(s, trackBoxX, p.pos.length, track.GetWidth(), style) // bottom line

		// display the header
		for j, c := range track.header {
			s.SetContent(trackBoxX+j, p.pos.y+1, c, nil, style)
		}

		track.DisplayTrack(s, trackBoxX, trackBoxY, style, theme)

		trackBoxX = trackBoxX + track.GetWidth()
		p.drawVLine(s, trackBoxX, p.pos.y, p.pos.length, style, "middle")
		trackBoxX += 1
	}
	p.drawVLine(s, trackBoxX-1, p.pos.y, p.pos.length, style, "right")
}

// TODO: Move the following out of the Pattern class. Will be useful for other sections of the screen.
// Draw a Horizontal line.
func (p *Pattern) drawHLine(s tcell.Screen, x, y, width int, style tcell.Style) {
	for j := 0; j < width; j++ {
		s.SetContent(x+j, y, tcell.RuneHLine, nil, style)
	}
}

// Draws a vertical Line
func (p *Pattern) drawVLine(s tcell.Screen, x, y, length int, style tcell.Style, sec string) {
	var now = time.Now()
	rand.Seed(now.Unix())
	var top, intersect, bottom rune
	line := tcell.RuneVLine
	switch sec {
	case "left":
		top = tcell.RuneULCorner
		bottom = tcell.RuneLLCorner
		intersect = tcell.RuneLTee
	case "middle":
		top = tcell.RuneTTee
		bottom = tcell.RuneBTee
		intersect = tcell.RunePlus
	case "right":
		top = tcell.RuneURCorner
		bottom = tcell.RuneLRCorner
		intersect = tcell.RuneRTee
	default:
		top = line
		bottom = line
		intersect = line
	}
	for i := 0; i <= length; i++ {
		switch i {
		case 0:
			s.SetContent(x, y+i, top, nil, style)
		case 2:
			s.SetContent(x, y+i, intersect, nil, style)
		case length:
			s.SetContent(x, y+i, bottom, nil, style)
		default:
			s.SetContent(x, y+i, line, nil, style)
		}
	}
}

//func (p *Pattern) RandomizeCell(curX, curY int) {
//	track := p.activeTrack(curX)
//	track.Randomize(curX, curY)
//}

// Returns the Active Track
func (p *Pattern) activeTrack() track {
	return p.tracks[p.cursor.track]
}

// Returns the Active Row
func (p *Pattern) activeRow() row {
	track := p.activeTrack()
	return track.rows[p.cursor.row]
}

// Returns the dimensions of the pattern window.
func (p *Pattern) GetSize() (int, int, int, int) {
	return p.pos.x, p.pos.y, p.pos.width, p.pos.length
}

// Returns dimension of Pattern Window including header and row numbers.
func (p *Pattern) GetTrackWindowSize() (int, int, int, int) {
	return p.pos.x + RowNumWidth, p.pos.y + HeaderSize, p.pos.width, p.rows
}

// Navigate to the right Column
func (p *Pattern) NavColRight() {

}
