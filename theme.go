package main

// Sets all the color values for theming the input.

// TODO: Set a 16 color pallete that can be swapped on the fly for custom theming.
// TODO: add urx mode for colors to support pywal and system-wide theming.
import (
	"github.com/gdamore/tcell"
)

var colors = map[string]tcell.Color{
	"NOT": tcell.NewHexColor(0xFFD166),
	"ST":  tcell.NewHexColor(0x06D6A0),
	"VL":  tcell.NewHexColor(0x118AB2),
	"FX":  tcell.NewHexColor(0xF7F7F9),

	"ActiveRow":   tcell.NewHexColor(0x646464),
	"WeakRow":     tcell.NewHexColor(0x151515),
	"StrongRow":   tcell.NewHexColor(0x0e0e0e),
	"StrongerRow": tcell.NewHexColor(0x080808),
}
